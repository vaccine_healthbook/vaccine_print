import moment from "moment";
import bodyParser from "body-parser";
import express from "express";
import md5 from "md5";
import cors from "cors";
import queryfn from "./app/query";
import querymulti from "./app/querymulti";

const server = express();
server.use(
    cors(),
    bodyParser.json(),
    bodyParser.urlencoded({
        extended: true
    })
);
WebApp.connectHandlers.use(Meteor.bindEnvironment(server));



server.post("/checklogin", (req, res) => {
  const {
      body
  } = req;
  const query = `select PERSON.*,REGISTER.RULE,REGISTER.NICKNAME,REGISTER.USERNAME from REGISTER LEFT JOIN PERSON ON REGISTER.CID = PERSON.CID  where username='${
  body.mailName
}' and password='${md5(body.password)}'`;
  //console.log(query);
  queryfn(res, query);
});

server.post("/searchperson", (req, res) => {
  const {
      body
  } = req;
  const query = `SELECT EPI_HEALTBOOK.*,PERSON.PRENAME as 'personprename',PERSON.NAME as 'personname',PERSON.LNAME 'personlastname',PERSON.BIRTH as 'personbirth',PERSON.NATION as 'personnation',PERSON.SEX as 'personsex',HOSPITAL.HOSPNAME as 'hospname',VACCINE.* FROM ((health_book.EPI_HEALTBOOK join PERSON on EPI_HEALTBOOK.CID = PERSON.CID) join HOSPITAL on EPI_HEALTBOOK.HOSPCODE = HOSPITAL.HOSPCODE) join VACCINE on EPI_HEALTBOOK.VACCINETYPE = VACCINE.VACCINECODE where EPI_HEALTBOOK.CID = '${body.cid}' ORDER BY EPI_HEALTBOOK.D_UPDATE;`;
  //console.log(query);
  queryfn(res, query);
});

server.post("/select_vaccine", (req, res) => {
  const {
      body
  } = req;
  const query = `select * from VACCINE`;
  //console.log(query);
  queryfn(res, query);
});

server.post("/add_vaccine", (req, res) => {
  const {
      body
  } = req;
  // console.log({body})
  const query = `INSERT INTO EPI_HEALTBOOK (CID, HOSPCODE, DATE_SERV, VACCINETYPE, D_UPDATE, MANUFACTURER, LOT, VACCINE_FROM, VACCINE_UNTIL) VALUE ('${body.CID}','${body.HOSPCODE}','${body.DATE_SERV}','${body.VACCINETYPE}','${body.D_UPDATE}','${body.MANUFACTURER}','${body.LOT}','${body.VACCINE_FROM}','${body.VACCINE_UNTIL}')`;
  console.log(query);
  queryfn(res, query);
});
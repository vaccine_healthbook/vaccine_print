import mysql from 'mysql'

const mysqlconfig = {
    host: Meteor.settings.public.sqlhost,
    user: Meteor.settings.public.sqluser,
    password: Meteor.settings.public.sqlpassword,
    database: Meteor.settings.public.sqldatabase,
    insecureAuth: true
}


const querymulti = (res, query, values) => {
    return new Promise(function(resolve, reject) {
        const con = mysql.createConnection(mysqlconfig)
        con.connect(function(err) {
            con.query(query, [values], function(err, result, fields) {

                if (err) {
                    //console.log({ "errorCode": err.code })
                    res.send({ "errorCode": err.code })
                } else {
                    resolve(result)

                    res.send(result)

                }
                con.end();
            })
        })
    })
}

export default querymulti
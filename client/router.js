Router.route('/', function() {
    Router.go('/login')
});

Router.route('/loader', function() {
    this.render('loader');
});

Router.route('/report_TH', function() {
    this.render('report_TH');
});

Router.route('/login', function() {
    if(Session.get('login_data')){
        this.render('dashboard');
    }else{
        this.render('login');
    }
});

Router.route('/dashboard', function() {
    if(Session.get('login_data')){
        this.render('dashboard');
    }else{
        this.render('login');
    }
});

Router.route('/report', function() {
    if(Session.get('login_data')){
        this.render('report');
    }else{
        this.render('login');
    }
});
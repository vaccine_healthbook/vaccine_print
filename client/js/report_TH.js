const axios = require("axios");

Template.report_TH.helpers({
    getHeadDiv() {
        return $(window).height() / 2
    },
    getDataLogin() {
        if (Session.get('login_data')) {
            return Session.get('login_data')
        }
    },
    getDataVaccine() {
        if (Session.get('vaccine_data')) {
            return Session.get('vaccine_data')
        }
    },
    getDataPerson() {
        if (Session.get('vaccine_data')) {
            return Session.get('vaccine_data')[0]
        }
    },
    getNameVaccine() {
        if (Session.get('vaccine_name')) {
            return Session.get('vaccine_name')
        }
    },
    getTimeStamp() {
        return moment().format('DD/MM/YYYY, hh:mm A');
    },
});

Template.report_TH.onRendered(function () {
    window.print();
    M.toast({
        html: 'พิมพ์เอกสารเรียบร้อยแล้ว'
    })
    window.history.back();
});

Template.report_TH.events({
    
});
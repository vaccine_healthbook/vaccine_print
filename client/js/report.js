const axios = require("axios");

function open_loader() {
    $('#loader').show()
    $('#content').hide()
}

function close_loader() {
    $('#loader').hide()
    $('#content').fadeIn(400)
}

function getHOSPID(x) {
    let str = x.split('[')
    let str2 = str[1].split(']')
    return str2[0]
}

Template.report.helpers({
    create: function () {

    },
    rendered: function () {

    },
    destroyed: function () {

    },
    getHeadDiv() {
        return $(window).height() / 2
    },
    getDataLogin() {
        if (Session.get('login_data')) {
            return Session.get('login_data')
        }
    },
    getDataVaccine() {
        if (Session.get('vaccine_data')) {
            return Session.get('vaccine_data')
        }
    },
    getDataPerson() {
        if (Session.get('vaccine_data')) {
            return Session.get('vaccine_data')[0]
        }
    },
    getNameVaccine() {
        if (Session.get('vaccine_name')) {
            return Session.get('vaccine_name')
        }
    }
});

Template.report.onRendered(function () {
    open_loader()
    setTimeout(function () {
        axios.post(`${Meteor.settings.public.api_host}/select_vaccine`, {}).then(function (response) {
                if (response.data.length > 0) {
                    Session.set('vaccine_name', response.data)
                    $('.modal').modal();
                    $('.datepicker').datepicker();
                    $('select').formSelect();
                    close_loader()
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }, 500)
});

Template.report.events({
    'click #add_vaccine'() {
        if ($('#name').val() != 'blank' && $('#name').val() != null) {
            open_loader()
            axios.post(`${Meteor.settings.public.api_host}/add_vaccine`, {
                    CID: Session.get('vaccine_data')[0].CID,
                    HOSPCODE: getHOSPID($('#hosp').val()),
                    DATE_SERV: moment($('#date').val()).format('YYYY-MM-DD'),
                    VACCINETYPE: $('#name').val(),
                    D_UPDATE: new Date(),
                    MANUFACTURER: $('#manufacture').val(),
                    LOT: $('#manufacture_number').val(),
                    VACCINE_FROM: moment($('#manufacture_date').val()).format('YYYY-MM-DD'),
                    VACCINE_UNTIL: moment($('#expire_date').val()).format('YYYY-MM-DD'),
                    CERTIFICATEL: ''
                }).then(function (response) {
                    axios.post(`${Meteor.settings.public.api_host}/searchperson`, {
                            cid: Session.get('vaccine_data')[0].CID
                        }).then(function (response) {
                            if (response.data.length > 0) {
                                Session.set('vaccine_data', response.data)
                            }
                            $('#date').val('')
                            $('#manufacture').val('')
                            $('#manufacture_number').val('')
                            $('#manufacture_date').val('')
                            $('#expire_date').val('')
                            $('#date').removeClass('valid')
                            $('#manufacture').removeClass('valid')
                            $('#manufacture_number').removeClass('valid')
                            $('#manufacture_date').removeClass('valid')
                            $('#expire_date').removeClass('valid')
                            $('#label-date').removeClass('active')
                            $('#label-manufacture').removeClass('active')
                            $('#label-manufacture_number').removeClass('active')
                            $('#label-manufacture_date').removeClass('active')
                            $('#label-expire_date').removeClass('active')
                            $('.modal.open').modal('close')
                            close_loader()
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                })
                .catch(function (error) {
                    console.log(error);
                });
        } else {
            M.toast({
                html: 'กรุณาเลือกวัคซีน'
            })
        }
    },
    // 'click #remove'(){
    //     $('.remove').fadeToggle(200)
    // },
    // 'click .remove_this'(){
    //     console.log(this)
    // }
    'click #back'(){
        Router.go('/dashboard')
    },
    'click #print'(){
        Router.go('/report_TH')
    },
});
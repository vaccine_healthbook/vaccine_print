const axios = require("axios");

function open_loader() {
    $('#loader').show()
    $('#content').hide()
}

function close_loader() {
    $('#loader').hide()
    $('#content').fadeIn(400)
}

Template.dashboard.helpers({ 
    create: function() { 
         
    }, 
    rendered: function() { 
         
    }, 
    destroyed: function() { 
         
    },
    getHeadDiv(){
        return $( window ).height()/8
    },
    getDataLogin(){
        if(Session.get('login_data')){
            return Session.get('login_data')
        }
    }
}); 

Template.dashboard.onRendered(function() {
    open_loader()
    close_loader()
});

Template.dashboard.events({ 
    'click #search'(){
        open_loader()
        axios.post(`${Meteor.settings.public.api_host}/searchperson`, {
            cid: $('#cid').val()
        }).then(function (response) {
            if(response.data.length > 0){
                Session.set('vaccine_data', response.data)
                Router.go('/report')
            }else{
                M.toast({
                    html: 'ไม่พบหมายเลขประจำตัวประชาชนนี้ในระบบ'
                })
                close_loader()
            }
        })
        .catch(function (error) {
            console.log(error);
        });
    }
}); 

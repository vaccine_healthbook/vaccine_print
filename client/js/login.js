const axios = require("axios");

Template.login.helpers({ 
    create: function() { 
         
    }, 
    rendered: function() { 
         
    }, 
    destroyed: function() { 
         
    },
    getHeadDiv(){
        return $( window ).height()/8
    } 
}); 

Template.login.onRendered(function() {
    console.log();
  });

Template.login.events({ 
    'click #login'(){
        axios.post(`${Meteor.settings.public.api_host}/checklogin`, {
            mailName: $('#username').val(),
            password: $('#password').val()
        }).then(function (response) {
            console.log(response.data)
            if(response.data.length > 0){
                Session.set('login_data', response.data[0])
                Router.go('/dashboard')
            }
            
        })
        .catch(function (error) {
            console.log(error);
        });
    }
}); 

Template.registerHelper("DateTime", function (data) {
    if (data && data != '0000-00-00') {
        var dm = moment(data).format("DD/MM/");
        var y = parseInt(moment(data).format("YYYY")) + 543 - 2500;
        var h = moment(data).format(" HH:mm");
        var date = dm + y;
        return date;
    } else {
        return "-";
    }
});

Template.registerHelper("sex", function (data) {
    if (data == '1') {
        return "ชาย";
    } else {
        return "หญิง";
    }
});

Template.registerHelper("nation", function (data) {
    if (data) {
        return data;
    } else {
        return "-";
    }
});